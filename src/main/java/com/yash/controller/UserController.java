package com.yash.controller;



import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yash.command.Registration;
import com.yash.model.User;
import com.yash.model.UserInfo;
import com.yash.service.UserInfoService;
import com.yash.service.UserService;

@Controller
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserInfoService userinfoService;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showUserRegistrationForm(Model model) {
		Registration register = new Registration();
		model.addAttribute("command", register);
		return "registration";
	}
	
	

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String processUserRegistration(@ModelAttribute("command") @Valid Registration registration,
			BindingResult result) {
		User userExists = userService.findUserByName(registration.getUserName());
		if (userExists != null) {
			result.rejectValue("userName", "error.user",
					"There is already a user registered with the userName provided");
		}
		if (result.hasErrors()) {
			System.out.println(result.getAllErrors());
			return "registration";
		}
		User user = new User();
		user.setEmail(registration.getEmail());
		user.setPassword(registration.getPassword());
		user.setUserName(registration.getUserName());
		userService.save(user);
		return "redirect:/login";
	}

	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public String showUserLoginForm() {
		return "login";
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByName(auth.getName());
		UserInfo userInfo =userinfoService.findUserInfoByUser(user);
		if(userInfo == null) {
			userInfo = new UserInfo();
		}
		model.addAttribute("userinfo", userInfo);
		return "welcome";
	}
	@RequestMapping(value = "/addInfo", method = RequestMethod.POST)
	public String processUserInfo(@ModelAttribute("command") @Valid UserInfo userInfo,
			BindingResult result){
		if(result.hasErrors()) {
			return "welcome";
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByName(auth.getName());
		userInfo.setUser(user);
		userinfoService.save(userInfo);
		
		return "redirect:/info";
	}
	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public String showuserInfo(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByName(auth.getName());
		UserInfo userInfo =userinfoService.findUserInfoByUser(user);
		model.addAttribute("userinfo", userInfo);
		return "userInfo";
	}
	
	

}
