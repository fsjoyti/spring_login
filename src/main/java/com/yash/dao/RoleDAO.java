package com.yash.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yash.model.Role;
import com.yash.model.User;
@Repository
public interface RoleDAO extends JpaRepository<Role,Integer>{
	Role findByRole(String role);

}
