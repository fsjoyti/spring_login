package com.yash.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yash.model.User;
import com.yash.model.UserInfo;

public interface UserInfoDAO extends JpaRepository<UserInfo,Integer>{
	UserInfo findByUser(User user);

}
