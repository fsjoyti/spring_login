package com.yash.service;

import com.yash.model.User;
import com.yash.model.UserInfo;

public interface UserInfoService {
	public void save(UserInfo info);
	public UserInfo findUserInfoByUser(User user);

}
