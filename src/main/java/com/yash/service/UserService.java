package com.yash.service;



import com.yash.model.User;

public interface UserService {
	public void save(User user);
	public User findUserByName(String userName);
}
