package com.yash.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yash.dao.UserInfoDAO;
import com.yash.model.User;
import com.yash.model.UserInfo;
import com.yash.service.UserInfoService;
@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService{
	@Autowired
	private UserInfoDAO userInfoDAO;

	@Override
	public void save(UserInfo info) {
		// TODO Auto-generated method stub
		userInfoDAO.save(info);
		
	}

	@Override
	public UserInfo findUserInfoByUser(User user) {
		// TODO Auto-generated method stub
		return userInfoDAO.findByUser(user);
	}
	
	
	

}
